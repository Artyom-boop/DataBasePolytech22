import psycopg2
from faker import Faker

con = psycopg2.connect(
    database="car_service",
    user="postgres",
    password="qwerty",
    host="localhost",
    port="5432"
)

print("Database opened successfully")
cur = cur = con.cursor()

cur.execute('''
create table Users
(
	userId serial primary key not null,
	login varchar(50) not null,
	passwordUser varchar(50) not null,
	isActive boolean not null
);

create table Mechanics
(
	mechanicId serial primary key not null,
	userId serial references Users(userId) not null,
	SNP varchar(50) not null,
	dateOfBirth date not null
);

create table Clients
(
	clientId serial primary key not null,
	userId serial references Users(userId) not null,
	SNP varchar(50) not null,
	DateOfBirth date not null
);

create table Cars
(
	carId serial primary key not null,
	clientId serial references Clients(clientId) not null,
	VIN varchar(15) not null,
	modelCar varchar(30) not null
);

create table ProgressStatuses
(
	progressStatusId serial primary key not null,
	status boolean not null,
	Description text not null
);

create table Orders
(
	orderId serial primary key not null,
	carId serial references Cars(carId) not null,
	mechanicId serial references Mechanics(mechanicId) not null,
	progressStatusId serial references ProgressStatuses(progressStatusId) not null,
	startDate date not null,
	finishDate date not null,
	price integer not null
);

create table Services
(
	serviceId serial primary key not null,
	nameService varchar(30) not null,
	price integer not null
);

create table if not exists ServiceInOrder
(
	ServiceInOrderId serial primary key not null,
	orderId serial references Orders(orderId) not null,
	serviceId serial references Services(serviceId) not null
);

''')
print("Operation to create tables completed successfully")

# parameters
number_of_service = 10
number_of_user = 20
number_of_client = 10
number_of_car = 10
number_of_mechanic = 10
number_of_order = 10

#


# author
fake = Faker("ru_RU")
for index in range(1, number_of_service + 1):
    nameService = fake.random.choice(["tire service", "car wash"])
    price = 1000
    if nameService == "car wash":
        price = 500
    nameService += str(index)
    command = f"insert into Services (serviceId, nameservice, price) values({index}, '{nameService}', {price});"
    print(command)
    cur.execute(f''' {command} ''')

# users
for index in range(1, number_of_user + 1):
    login = fake.name()
    password = fake.name()
    command = f"insert into Users (userId, login, passwordUser, isActive) values " \
              f"({index}, '{login}', '{password}', {True})"
    print(command)
    cur.execute(f''' {command} ''')

# client
for index in range(1, number_of_client + 1):
    name = fake.name()
    password = fake.name()
    date = fake.date_of_birth()
    command = f"insert into Clients (clientId, userId, SNP, dateOfBirth) values " \
              f"({index}, {index}, '{name}', cast('{date}' as date))"
    print(command)
    cur.execute(f''' {command} ''')

# mechanic
for index in range(1, number_of_mechanic + 1):
    date = fake.date_of_birth()
    name = fake.name()
    command = f"insert into Mechanics (mechanicId, userId, SNP, dateOfBirth) values ({index}, {index + 10}, '{name}', cast('{date}' as date))"
    print(command)
    cur.execute(f''' {command} ''')

# car
for index in range(1, number_of_car + 1):
    name = fake.name()
    vin = fake.random.choice(["2412525342", "868757544", "64624624", "97967357357"])
    password = fake.name()
    car = fake.random.choice(["Toyota", "Chevrolet", "Renault", "Volkswagen"])
    command = f"insert into Cars (carId, clientId, VIN, modelCar) values ({index}, {index}, '{vin}', '{car}')"
    print(command)
    cur.execute(f''' {command} ''')

# progressStatus
for index in range(1, number_of_car + 1):
    description = fake.text()
    command = f"insert into progressStatuses (progressStatusId, status, description) values " \
              f"({index}, {True}, '{description}')"
    print(command)
    cur.execute(f''' {command} ''')

# order
for index in range(1, number_of_order + 1):
    date = fake.date_of_birth()
    command = f"insert into Orders (orderId, carId, mechanicId, progressStatusId, startDate, finishDate, price) values" \
              f" ({index}, {index}, {index}, {index}, cast('{date}' as date), cast('{date}' as date), 1000)"
    print(command)
    cur.execute(f''' {command} ''')

# serviceInOrder
for index in range(1, number_of_order + 1):
    command = f"insert into serviceInOrder (serviceInOrderId, orderId, serviceId) values" \
              f" ({index}, {index}, {index})"
    print(command)
    cur.execute(f''' {command} ''')

con.commit()
con.close()
