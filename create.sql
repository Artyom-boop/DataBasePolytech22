create table Users
(
	userId serial primary key not null,
	login varchar(20) not null,
	passwordUser varchar(20) not null,
	isActive boolean not null
);

create table Mechanics
(
	mechanicId serial primary key not null,
	userId serial references Users(userId) not null,
	SNP varchar(50) not null,
	dateOfBirth date not null
);

create table Clients
(
	clientId serial primary key not null,
	userId serial references Users(userId) not null,
	SNP varchar(50) not null,
	DateOfBirth date not null
);

create table Cars
(
	carId serial primary key not null,
	clientId serial references Clients(clientId) not null,
	VIN varchar(15) not null,
	modelCar varchar(30) not null
);

create table ProgressStatuses
(
	progressStatusId serial primary key not null,
	status boolean not null,
	Description text not null
);

create table Orders
(
	orderId serial primary key not null,
	carId serial references Cars(carId) not null,
	mechanicId serial references Mechanics(mechanicId) not null,
	progressStatusId serial references ProgressStatuses(progressStatusId) not null,
	startDate date not null,
	finishDate date not null,
	price integer not null
);

create table Services
(
	serviceId serial primary key not null,
	nameService varchar(30) not null,
	price integer not null
);

create table ServiceInOrder
(
	ServiceInOrderId serial primary key not null,
	orderId serial references Orders(orderId) not null,
	serviceId serial references Services(serviceId) not null
);