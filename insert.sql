insert into Services (serviceId, nameservice, price) values 
(1, 'tire service', 1000),
(2, 'car wash', 500);

insert into Users (userId, login, passwordUser, isActive) values
(1, 'Nick', 'aa', TRUE),
(2, 'Jack', 'aa', TRUE);

insert into Clients (clientId, userId, SNP, dateOfBirth) values
(1, 2, 'Jack Jenkins', '2000-9-15');

insert into Mechanics (mechanicId, userId, SNP, dateOfBirth) values
(1, 1, 'Nick Hammer', '1989-11-12');

insert into Cars (carId, clientId, VIN, modelCar) values
(1, 1, '1232354657', 'Toyota Camry');

insert into progressStatuses (progressStatusId, status, description) values
(1, true, 'The job is done');

insert into Orders (orderId, carId, mechanicId, progressStatusId, startDate, finishDate, price) values
(1, 1, 1, 1, '2022-4-15', '2022-4-16', 1000);

insert into serviceInOrder (serviceInOrderId, orderId, serviceId) values
(1, 1, 1);
